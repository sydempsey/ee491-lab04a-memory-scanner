///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491  - Software Reverse Engineering
/// Lab 04a - Mem Scanner
///
/// @file memoryscan.c
/// @version 1.0
///
/// @author Sydney Dempsey <dempseys@hawaii.edu>
/// @brief  Lab 04a - Memory Scanner - EE 491F
/// @date   09 Feb 2021
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

enum WordCheck { WHITESPACE, INWORD };
enum Field { ADDRESS, PERM, OFFSET, DEV, INODE, PATH };

struct MemTable{
   char Add[4000];
   char* aBegin;
   char* aEnd;
   char Perm[1048];
   char Off[1048];
   char Dev[1048];
   char Inode[1048];
   char Path[1048];
   void* vABegin;
   void* vAEnd;
   int numBytes;
   int numA;
};

struct MemTable memT[1000];
enum WordCheck wordCheck = 8;
enum Field field = ADDRESS;

FILE* f = NULL;
int numReadable = 0;
int CharCheck;

/// Examples: (in /proc/self/maps)
/// Address                   perm  offset   dev   inode                      path
/// 55cb8756b000-55cb87598000 r--p 00000000 08:04 1574849                    /usr/bin/bash
/// 55cb87598000-55cb87669000 r-xp 0002d000 08:04 1574849                    /usr/bin/bash
/// 55cb87669000-55cb876a2000 r--p 000fe000 08:04 1574849                    /usr/bin/bash
/// 55cb876a2000-55cb876a6000 r--p 00136000 08:04 1574849                    /usr/bin/bash
/// 55cb876a6000-55cb876af000 rw-p 0013a000 08:04 1574849                    /usr/bin/bash
/// 55cb876af000-55cb876b9000 rw-p 00000000 00:00 0 
/// 55cb8832a000-55cb88494000 rw-p 00000000 00:00 0                          [heap]
/// 7f3ff0ecf000-7f3ffe3ff000 r--p 00000000 08:04 1838829                    /usr/lib/locale/locale-archive
/// 7f3ffe3ff000-7f3ffecd3000 r--s 00000000 08:07 131095                     /var/lib/sss/mc/passwd
/// 7f3ffecd3000-7f3ffecd5000 r--p 00000000 08:04 1588322                    /usr/lib64/libnss_sss.so.2

int WhiteSpace(int currChar){
   if( (currChar == ' ') || (currChar == '\n') || (currChar == '\t') || (currChar == '\0') || (currChar == '\r') )
      return TRUE;
   else return FALSE;
}


// Function reads file and stores respective values in memT[].FIELD
void ParseFile(){
   char currChar = fgetc(f);
   char currField[100];
   int count = 0;

   while( currChar != EOF ){
      // printf("%c", currChar);
       
      // Check if in a field or not
      if ( wordCheck == 8 ){
         if( WhiteSpace(currChar) ){
            wordCheck = WHITESPACE;
         }
         else wordCheck = INWORD;
      }
      if( !WhiteSpace(currChar) ){
               wordCheck = INWORD;
            }

      //Store Current Field Character
      if ( !WhiteSpace(currChar) && wordCheck == INWORD){
         currField[count] = currChar;
         //printf("%c", currChar);
         count++;
      }

      //Reached end of field
      else if( WhiteSpace(currChar) && wordCheck == INWORD ){
         if( field == ADDRESS ){
            strcpy( memT[numReadable].Add, currField );
            field++;
           // printf("\n\nleft address!! \n\n");
         }
         else if( field == PERM ){
            strcpy( memT[numReadable].Perm, currField );
            field++;

           // printf("\n\nleft PERMISSIONS \n");
         }
         else if( field == OFFSET ){
            strcpy( memT[numReadable].Off, currField );
            field++;
          //  printf("\n\nleft OFFSET \n");
         }
         else if( field == DEV ){
            strcpy( memT[numReadable].Dev, currField );
            field++;
           // printf("\nleft DEV \n");
         }
         else if( field == INODE){
            strcpy( memT[numReadable].Inode, currField );
            field++;
           // printf("\nleft INODE! \n");
         }
         else if( field == PATH){
            if(currField[0] == '/' || currField[0] == '['){
               strcpy( memT[numReadable].Path, currField );
               field++;
             //  printf("\nleft PATH! \n");
            }
            else{
               //printf("\n\n NO PATH. Skipping to next line!");
               field = ADDRESS + 1;
               strcpy( memT[numReadable].Add, currField );
               //memT[numReadable].Path = '\0';
            }
         }
         count = 0;
         wordCheck = WHITESPACE;
         memset( currField, '\0', 100 );
         currChar = fgetc(f);
         continue;
      }


      if( field == 6){
         numReadable++;
         field = ADDRESS;
         count = 0;
      }
      
      //skip whitespace
      if( WhiteSpace(currChar) ){
         wordCheck = WHITESPACE;
         currChar = fgetc(f);
         count = 0;
         continue;
      }

      //update wordCheck
      if( !WhiteSpace(currChar) ){
         wordCheck = INWORD;
      }
     
      currChar = fgetc(f);
      // printf("\n wordcheck: %d \t current field: %s", wordCheck, currField);
   }
}


// Splits up address1 and address2 while strings
// Converts strings to void*
void Conversions(){

   int check = 0;
   int count = 0;
   int count2 = 0;
   int bytes = 0;
   char addOne[100];
   char addTwo[100];

   for(int i = 0; i < numReadable; i++){
      
      //Start Address
      while( memT[i].Add[count] != '-'){
         addOne[count] = memT[i].Add[count];
         count++;
      }

      count++;

      //End Address
      while( memT[i].Add[count] != '\0' ) {
         addTwo[count2] = memT[i].Add[count];
         count++;
         count2++;
      }

      memT[i].aBegin = addOne;
      memT[i].aEnd = addTwo;
     // printf("\n\n Strings: Add1 = %s \t\t Add2 = %s\n\n", memT[i].aBegin, memT[i].aEnd);
      
      sscanf( memT[i].aBegin, "%p", &(memT[i].vABegin) );
      sscanf( memT[i].aEnd, "%p", &(memT[i].vAEnd) );

   //   printf("Void*: AddBegin = %p \t\t AddEnd = %p\n", memT[i].vABegin, memT[i].vAEnd);
      count = count2 = 0;
      memset( addOne, '\0', 100);
      memset( addTwo, '\0', 100);
   }
}

// Checks permissions and performs byte and 'A' counts
void Counts(){
   
   int numB = 0;
   int numbA = 0;
   
   for(int i = 0; i < 7; i++){
      
      if( memT[i].Perm[0] != 'r' ) continue;
       // printf("\n Perms: %s\t", memT[i].Perm);
      
      for ( void* k = memT[i].vABegin; k < memT[i].vAEnd; k++ ){
         if( *(char*)k == 'A' ){
            numbA++;
         }
         numB++;
         //printf("Number Bytes: %d", numB);
      }

      memT[i].numBytes = numB;
      memT[i].numA = numbA;
     // printf("\t [%d]: Num Bytes = %d \t Num A's = %d",i, memT[i].numBytes, memT[i].numA);
      numB = 0; numbA = 0;
   }
}

// Prints Final Output for Memory Scanner
void Print(){
   
   for(int i = 0; i < numReadable; i++){
   
      printf("\n%d:  %p - %p   %s   Number of bytes read [%d]   Number of 'A' is [%d]\n", i, memT[i].vABegin, memT[i].vAEnd, memT[i].Perm, memT[i].numBytes, memT[i].numA);

      //printf("\n%d:  %s - %s   %s   Number of bytes read [%d]   Number of 'A' is [%d]\n", i, memT[i].aBegin, memT[i].aEnd, memT[i].Perm, memT[i].numBytes, memT[i].numA);
   }

}

// Main part of Mem Scanner
int main( int argc, char* argv[] ){
   printf("Begin Memory Scanner: \n");
   
   f = fopen( "/proc/self/maps", "r" );
   if(f == NULL){
      printf("memoryscanner: Unable to open '/proc/self/maps'. \n");
      return EXIT_FAILURE;
   }
   
   ParseFile();
   
   Conversions();
   
   Counts();
  
   Print();

   fclose(f);
   
   return EXIT_SUCCESS;
}
