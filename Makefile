all: clean memoryscan test

test: memoryscan
		./memoryscan

memoryscan: memoryscan.c
		gcc -o memoryscan memoryscan.c

clean:
		rm -f memoryscan *.o
